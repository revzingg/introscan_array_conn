INTEGER-PLACES         3
DECIMAL-PLACES         5
X-OFFSET               0.000000
Y-OFFSET               0.000000
FEEDRATE               1
COORDINATES            ABSOLUTE
OUTPUT-UNITS           METRIC
TOOL-ORDER             INCREASING
REPEAT-CODES           YES
SUPPRESS-LEAD-ZEROES   NO
SUPPRESS-TRAIL-ZEROES  NO
SUPPRESS-EQUAL         NO
TOOL-SELECT            YES
OPTIMIZE_DRILLING      YES
ENHANCED_EXCELLON      NO
TAPE-FILE              C:/WORK_LOCAL/introscan_array_conn/board/m8212_conn_v1_0.drl
HEADER                 none
LEADER                 12
CODE                   ASCII
SEPARATE               NO
DRILLING               LAYER-PAIR
BACKDRILL              NO
CAVITY                 NO
